const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productSchema = new Schema({
    
        brand: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        productDescription: {
            type: String,
            required: true
        },
        productName: {
            type: String,
            required: true
        },
        productid: {
            type: String,
            required: true
        },
        imageUrl: {
            type: String,
            required: true
        }
    },    
{
    versionKey: false 
});

module.exports = mongoose.model('Product', productSchema);