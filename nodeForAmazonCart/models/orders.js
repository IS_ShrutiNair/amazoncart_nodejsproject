const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const orderSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    products: [{
        productid: { type: String, required: true },
        quantity: { type: Number, required: true },
        productName: { type: String, required: true },
        price: { type: Number, required: true },
    }],
    date: {
        type: Date,
        required: true
    }

});

module.exports = mongoose.model('Order', orderSchema);