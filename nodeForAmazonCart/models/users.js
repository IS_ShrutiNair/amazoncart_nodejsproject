const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    fullname: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    resetToken:String,
    resetTokenExpiration:Date,

    password: {
        type: String,
        required: true
    }

});

module.exports = mongoose.model('User', userSchema);