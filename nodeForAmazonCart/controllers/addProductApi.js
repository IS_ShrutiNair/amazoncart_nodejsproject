const express = require('express');
const router = express.Router();
const Product = require('../models/products');

exports.addProductApi = function (req, res, next) {
    console.log("in addProductApi", req.body.pid);

    const id = req.body.pid;
    const name = req.body.pname;
    const brand = req.body.pbrand;
    const desc = req.body.pdesc;
    const price = req.body.pprice;
    const image = '/images/'+req.file.filename  ;
    console.log(req.file);
    let product = null;  
    
        product = new Product({
            productid: id,
            productDescription: desc,
            productName: name,
            brand: brand,
            price: price,
            imageUrl:image
        });
       
        product.save()
                .then(result => {                  
                    console.log('Created Product');
                    res.redirect('http://localhost:3000/product');
                })
                .catch(err => {
                    console.log(err);
                });  
    
};
