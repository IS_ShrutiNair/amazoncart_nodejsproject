const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const User = require('../models/users');
const nodemailer = require('nodemailer');
const sendGridTransport = require('nodemailer-sendgrid-transport');
const transporter = nodemailer.createTransport(sendGridTransport({
    auth: {
        api_key: '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'
    }
}));

exports.resetPasswordApi = (req, res, next) => {
    console.log("in reset");
    console.log(req.body.email);

    crypto.randomBytes(32, (err, buffer) => {
        if (err) {
            console.log("eeeee", err);
            return res.redirect('http://localhost:3000/resetpassword');
        }
        const token = buffer.toString('hex');
        User.findOne({ username: req.body.email })
            .then(user => {
                if (!user) {
                    res.redirect('http://localhost:3000/resetpassword?reset=false');
                }
                user.resetToken = token;
                user.resetTokenExpiration = Date.now() + 3600000;
                return user.save();
            })
            .then(result => {
                res.redirect('http://localhost:3000/resetpassword?reset=true');
                return transporter.sendMail({
                    to: req.body.email,
                    from: 'amazon_cart@nodeproject.com',
                    subject: 'Password Reset',
                    html: `
            <p>You requested a password reset</p>
            <p>Click this <a href="http://localhost:3000/resetpassword/${token}">link</a> to set a new password</p>
            `
                });
            })
            .catch(err => {
                console.log(err);
            })

    });

};

