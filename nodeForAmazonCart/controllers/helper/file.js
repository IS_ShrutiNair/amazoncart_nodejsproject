const fs = require('fs');
const path = require('path');


const deleteFile = (filepath)=>{
   let newFilepath = path.join(__dirname,'../../../amazoncart/public', filepath);
    fs.unlink(newFilepath,(err)=>{
        if(err){
            throw (err);
        }
    })
}

exports.deleteFile = deleteFile;