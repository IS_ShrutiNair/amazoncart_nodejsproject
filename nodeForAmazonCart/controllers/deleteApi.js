const express = require('express');
const router = express.Router();
const Product = require('../models/products');
const filehelper = require('./helper/file');

exports.deleteApi = function (req, res, next) {
    console.log("in deleteApi");
    const pid = req.body.pid;
    console.log(pid);

    Product.findByIdAndRemove(pid)
    .then((product=>{
        filehelper.deleteFile(product.imageUrl);
        console.log('product ');
        console.log(product);
        res.json({ data: 1 });
    }))
        .catch(err => {
            console.log(err);
            res.redirect('http://localhost:3000/product');
        });
};
