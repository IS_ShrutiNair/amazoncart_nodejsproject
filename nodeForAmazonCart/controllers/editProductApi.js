const express = require('express');
const router = express.Router();
const Product = require('../models/products');
const filehelper = require('./helper/file');

exports.editProductApi = function (req, res, next) {
    console.log("in editProductApi", req.body.pid);

    const id = req.body.pid;
    const name = req.body.pname;
    const brand = req.body.pbrand;
    const desc = req.body.pdesc;
    const price = req.body.pprice;
  
    console.log(req.file);
    let product = null;

    
    Product.findOne({ productid: id })
        .then(prod => {           
            prod.productDescription = desc;
            prod.productName = name;
            prod.brand = brand;
            prod.price = price;
                if(req.file)
                {
                    filehelper.deleteFile(prod.imageUrl);
                    const image = '/images/' + req.file.filename;
                    prod.imageUrl = image
                }
              
            return prod.save();
        })
        .then(result => {
            res.redirect('http://localhost:3000/product');
        })
        .catch(err => {
            console.log(err);
        })

};
