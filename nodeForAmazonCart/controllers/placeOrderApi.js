const express = require('express');
const router = express.Router();
const Order = require('../models/orders');
const Product = require('../models/products');
exports.placeOrderApi = function (req, res, next) {
    console.log("in placeOrderApi");

    const username = req.body.username;
    const cart = req.body.cart;

    console.log("username");
    console.log(username);
    console.log("cart");
    console.log(cart);
    let order = null;
    let products = [];

    var promise = new Promise(function(resolve, reject) { 
        Object.entries(cart).map((el) => {         
            Product.findOne({ productid: el[0] })
            .then(product => {             
                let prod = {
                    productid: el[0],
                    quantity: el[1],
                    productName:product.productName,
                    price:product.price
                };   
                products.push(prod);      
            })  
         });
         resolve();         
      }); 
        
      promise. 
          then(function () {           
            setTimeout(function(){
                order = new Order({
                    username: username,
                    products: products,
                    date: Date().toString()
                });
            
                order.save()
                    .then(result => {
                        console.log('Placed Order');
                        res.json({
                            data: 1
                        });
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }, 2000); 
        
          }). 
          catch(function () { 
              console.log('Some error has occured'); 
          }); 
};
