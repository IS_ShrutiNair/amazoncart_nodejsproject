const express = require('express');
const router = express.Router();
const Product = require('../models/products');

router.get('/', (req, res, next) => {
    console.log("in getProductsApi");
    console.log(Product);
    Product.find({}, function (err, products) {
        if (err) return console.error(err);

        res.json(products);
    });
});
router.get('/:id', (req, res, next) => {

    console.log("in getProductsApi id");
    console.log(req.params.id);

    Product.findOne({ productid: req.params.id }, function (err, products) {
        if (err) return console.error(err);
        res.json(products);
    });
});

module.exports = router;