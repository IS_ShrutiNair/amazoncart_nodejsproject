const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();
const PDFDocument = require('pdfkit');
const Order = require('../models/orders');
router.get('/invoiceApi/:id', (req, res, next) => {
    Order.findById(req.params.id)
        .then(ord => {
            if (!ord) {
                return new Error('Order not Found');
            }
            console.log("in invoiceApi id");
            console.log(req.params.id);
            const invoiceName = 'invoice-' + req.params.id + '.pdf';
            const invoicePath = path.join('../', 'amazoncart', 'public', 'invoices', invoiceName);
            const pdfDoc = new PDFDocument();

            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', 'attachment;filename="' + invoiceName + '"')
            pdfDoc.pipe(fs.createWriteStream(invoicePath));
            pdfDoc.pipe(res);
            pdfDoc.fontSize(26).text('Invoice', {
                underline: true
            });
            pdfDoc.text('----------------------------------------------');
            let totalPrice = 0;

            ord.products.forEach(prod => {
                totalPrice += prod.quantity * prod.price;
                pdfDoc.fontSize(14).text(
                    prod.productName + ' - ' + prod.quantity + ' x ' + 'Rs.' + prod.price
                );
            });
            pdfDoc.text('------------------');
            pdfDoc.text('Total Price : Rs.'+totalPrice);
            pdfDoc.end();
            // fs.readFile(invoicePath, (err, data) => {
            //     if (err) {
            //         return next(err);
            //     }
            //     res.setHeader('Content-Type', 'application/pdf');
            //     res.setHeader('Content-Disposition','attachment;filename="'+invoiceName+'"')
            //     res.send(data);
            // })
            // const file = fs.createReadStream(invoicePath);

            // file.pipe(res);
        })
        .catch(e => {
            console.log(e);
        })



});

module.exports = router;
