const express = require('express');
const router = express.Router();
const User = require('../models/users');
const bcrypt = require('bcryptjs');

exports.setNewPasswordApi = function(req, res, next) {
    console.log("in setNewPasswordApi");
    const newpassword = req.body.newpassword;
    const token = req.body.token;
    const tokenurl =  req.body.token + 1;
    let user = null;
  console.log(newpassword);
  console.log(token);
   
   User.findOne({resetToken:token,resetTokenExpiration:{$gt:Date.now()}})
        .then(u => {      
            user=u;  
           return bcrypt.hash(newpassword,12);           
        })
        .then(hashpassword =>{
            user.password = hashpassword;
            user.resetToken = null;
            user.resetTokenExpiration = undefined;
            user.save();
             console.log(user);
            console.log('updated password');
            res.redirect('http://localhost:3000/resetpassword/'+tokenurl);            
        })
        .catch(err => {
            console.log(err);
            res.redirect('http://localhost:3000/resetpassword/0');  
        });
};
