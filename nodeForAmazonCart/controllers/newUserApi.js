const express = require('express');
const router = express.Router();
const User = require('../models/users');
const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const sendGridTransport = require('nodemailer-sendgrid-transport');
const transporter = nodemailer.createTransport(sendGridTransport({
    auth: {
        api_key: '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'
    }
}));

exports.newUserApi = function (req, res, next) {
    console.log("in newUsersApi");
    const fullname = req.body.fullname;
    const password = req.body.password;
    const username = req.body.username;

    bcrypt.hash(password, 12)
        .then(hashpassword => {
            const user = new User({
                fullname: fullname,
                username: username,
                password: hashpassword
            });
            user.save()
        })
        .then(result => {
            console.log(result);
            console.log('Created User');
            res.redirect('http://localhost:3000/login');
            return transporter.sendMail({
                to: username,
                from: 'amazon_cart@nodeproject.com',
                subject: 'Signup succeeded!',
                html: '<h1>You successfully signed up!</h1>'
            });
        })
        .catch(err => {
            console.log(err);
        });
};
