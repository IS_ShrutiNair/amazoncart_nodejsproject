
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');
const port = 9000;
const app = express();
const multer = require('multer');
const getProductsApi = require('./controllers/getProductsApi');
const newUserApi = require('./controllers/newUserApi');
const loginApi = require('./controllers/loginApi');
const addProductApi = require('./controllers/addProductApi');
const resetPasswordApi = require('./controllers/resetPasswordApi');
const setNewPasswordApi = require('./controllers/setNewPasswordApi');
const deleteApi = require('./controllers/deleteApi');
const editProductApi = require('./controllers/editProductApi');
const placeOrderApi = require('./controllers/placeOrderApi');
const getOrderApi = require('./controllers/getOrderApi');
const invoiceApi = require('./controllers/invoiceApi');

const filestorage = multer.diskStorage({
    destination:(req,file,cb)=>{
        cb(null,'../amazoncart/public/images');
    },
    filename:(req,file,cb)=>{
        cb(null,file.originalname);
    }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer({storage:filestorage}).single('pimage'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'images')));
// app.use(express.static(path.join(__dirname, 'public')));
// console.log("__dirname");
// console.log(__dirname);
app.use(morgan("dev"));
// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });
const MONGODB_URI =
    'mongodb+srv://node_user:node_user@nodebasic-kmhgm.mongodb.net/amazon_cart';

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.get('/', cors(), getProductsApi);
app.get('/getOrderApi/:username', cors(), getOrderApi);
app.get('/:id', cors(), getProductsApi);
app.post('/loginApi', cors(), loginApi.loginApi);
app.post('/newUserApi', cors(), newUserApi.newUserApi);
app.post('/addProductApi', cors(), addProductApi.addProductApi);
app.post('/resetPasswordApi', cors(), resetPasswordApi.resetPasswordApi);
app.post('/setNewPasswordApi', cors(), setNewPasswordApi.setNewPasswordApi);
app.post('/deleteApi', cors(), deleteApi.deleteApi);
app.post('/editProductApi', cors(), editProductApi.editProductApi);
app.post('/placeOrderApi', cors(), placeOrderApi.placeOrderApi);
app.get('/invoiceApi/:id', cors(), invoiceApi);


mongoose
    .connect(MONGODB_URI)
    .then(result => {
        console.log("database connected");
    })
    .catch(err => {
        console.log(err);
    });

app.listen(port, () => {
    console.log("node server is starting on ", port);
});

