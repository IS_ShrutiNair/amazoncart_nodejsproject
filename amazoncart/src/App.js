import React from 'react';
import Layout from './Components/Layout/Layout';
import './App.css';
import { Router, Switch, Route } from "react-router";
import { createBrowserHistory } from "history";
import Cart from './Containers/Cart/Cart';
import ContentContainer from './Containers/ContentContainer/ContentContainer';
import Login from './Components/Login/Login';
import Signup from './Components/Signup/Signup';
import AddProduct from './Components/AddProduct/AddProduct';
import Product from './Components/Product/Product';
import ResetPassword from './Components/ResetPassword/ResetPassword';
import SetPassword from './Components/SetPassword/SetPassword';
import EditProduct from './Components/EditProduct/EditProduct';
import Order from './Components/Order/Order';
// import UserProvider from './UserContext';
function App() {
  // const user = {name:"Nair"}
  return (
    <div className="App">
      {/* <UserProvider value={user}> */}
      <Router history={createBrowserHistory()}>
        <Layout>
          <Switch>
            <Route exact path='/login' component={Login} />
            <Route exact path='/signup' component={Signup} />
            <Route exact path='/addproduct' component={AddProduct} />           
            <Route exact path='/product' component={Product} />
            <Route exact path='/home' component={ContentContainer} />
            <Route path='/category' component={ContentContainer} />
            <Route path='/cart' component={Cart} />
            <Route path='/order' component={Order} />
            <Route exact path='/resetpassword' component={ResetPassword} />
            <Route exact path='/resetpassword/:token' component={SetPassword} />
            <Route exact path='/editproduct/:id' component={EditProduct} />
          </Switch>
        </Layout>
      </Router>
      {/* </UserProvider> */}
    </div>
  );
}

export default App;
