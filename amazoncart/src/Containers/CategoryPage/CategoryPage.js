import React, { Component } from 'react';
import "primeflex/primeflex.css";
import './CategoryPage.css';
import MainContent from '../../Components/MainContent/MainContent';
import ProductDetail from '../ProductDetail/ProductDetail';
import {withRouter} from 'react-router-dom';
import axios from 'axios';

class CategoryPage extends Component {
    constructor() {
        super();
        this.state = {
            mobiles: [],
            laptops: [],
            cameras: [],
            allProductData: [],
            productDetailsClicked: false,
            selectedId: null
        }
    }
    
    componentDidMount() {
        axios.get('http://localhost:9000/')
            .then((response) => {              
                var mob = [];
                var lap = [];
                var cam = [];
                console.log(response.data);
                response.data.map((el) => {
                    if (el.productid.startsWith('cam')) {
                       cam.push(el);
                    }
                    if (el.productid.startsWith('mob')) {
                        mob.push(el);
                    }
                    if (el.productid.startsWith('lap')) {
                        lap.push(el);
                    }
                });
               
                this.setState({
                    mobiles: mob,
                    laptops: lap,
                    cameras: cam,
                    allProductData:[...mob,...lap,...cam]
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    productDetailsHandler = (id) => {
        this.setState({
            productDetailsClicked: true,
            selectedId: id
        });
        this.props.history.listen(location => {
            this.props.location.pathname = location.pathname;
        });
    }

    render() {    
        console.log("this.state.allProductData")        
        console.log(this.state.allProductData)        
        let categoryContent = null;
        let tempProductCategory = null;
        if (this.state.productDetailsClicked) {
            (this.state.allProductData).map(el => {        
                
                if ('mob'== this.state.selectedId.slice(0, 3))
                    tempProductCategory= 'mobiles';
                if ('cam' == this.state.selectedId.slice(0, 3))
                    tempProductCategory= 'cameras';
                if ('lap' == this.state.selectedId.slice(0, 3))
                    tempProductCategory= 'laptops';
            });
         
            let tempProductDetail =this.state.allProductData.filter(el => {
                return el.productid == this.state.selectedId
            });
           
            if (this.props.location.pathname == "/category/" + tempProductCategory + '/' + this.state.selectedId) {
                categoryContent = <ProductDetail data={tempProductDetail[0]} />
            }
           
        }



        if (this.props.location.pathname == "/category/mobiles") {
            categoryContent = <MainContent data={this.state.mobiles} heading={this.props.location.pathname.split('/')[2]} clicked={this.productDetailsHandler} />
        }
        if (this.props.location.pathname == "/category/laptops") {
            categoryContent = <MainContent data={this.state.laptops} heading={this.props.location.pathname.split('/')[2]} clicked={this.productDetailsHandler} />
        }
        if (this.props.location.pathname == "/category/cameras") {
            categoryContent = <MainContent data={this.state.cameras} heading={this.props.location.pathname.split('/')[2]} clicked={this.productDetailsHandler} />
        }   

        return (categoryContent);
    }
}

export default withRouter(CategoryPage);