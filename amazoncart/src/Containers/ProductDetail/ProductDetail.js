import React, { Component } from 'react';
import Button from '../../Components/Button/Button';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actionCreators from '../../Store/actions';
import Swal from 'sweetalert2';
import './ProductDetail.css';

class ProductDetail extends Component {

    addToCartClickHandler = (id) => {
        this.props.addToCartHandler(id);
        Swal.fire(
            {
                title: "Added to Cart!",
                icon: 'success'
            });
    }

    goBack = () => {
        this.props.history.goBack();
    }

    render() {      
        return (
            <div className="ProductDetail">          
                <img  className="product-image" src={'/images/' + (this.props.data.brand.toLowerCase()) + '.png'} alt={this.props.data.productName} />
                <Button displayText="Add to Cart" id={this.props.data.productid} clicked={() => this.addToCartClickHandler(this.props.data.productid)} />
                <p><b>Price:</b>{this.props.data.price}/-</p>
                <p><b>Product Description:</b>{this.props.data.productDescription}</p>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addToCartHandler: (id) => dispatch(actionCreators.addToCart(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductDetail));