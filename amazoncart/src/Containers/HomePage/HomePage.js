import React, { Component } from 'react';
import "primeflex/primeflex.css";
import './HomePage.css';
import { Carousel } from 'primereact/carousel';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/nova-light/theme.css';
import axios from 'axios';
import { NavLink } from 'react-router-dom';
// import UserContext, { UserConsumer } from '../../UserContext';

class HomePage extends Component {
    constructor() {
        super();
        this.state = {
            products: [],
            carousalProducts: []
        };
        this.productTemplate = this.productTemplate.bind(this);
        this.responsiveSettings = [{
            breakpoint: '1024px',
            numVisible: 3,
            numScroll: 3
        },
        {
            breakpoint: '768px',
            numVisible: 2,
            numScroll: 2
        },
        {
            breakpoint: '560px',
            numVisible: 1,
            numScroll: 1
        }
        ];
    }
    //  static contextType = UserContext;


    componentDidMount() {
        // const user = this.context;
        // console.log(user.name);
        axios.get('http://localhost:9000/')
            .then((response) => {
                console.log("response.dataa");
                console.log(response.data);
                this.setState({
                    products: response.data
                })
            })
            .catch(function (error) {
                console.log(error);
            })

    }

    productTemplate(product) {
        let category = null;
        if (product.productid.slice(0, 3) == 'cam') {
            category = 'cameras';
        }
        if (product.productid.slice(0, 3) == 'mob') {
            category = 'mobiles';
        }
        if (product.productid.slice(0, 3) == 'lap') {
            category = 'laptops';
        }
        return (
            <NavLink className="product-details" to={'/category/'+category}>
                <div className="product-heading" > {product.brand} </div>
                <div className="p-grid p-nogutter" >
                    <div className="p-col-12" >
                        <img className="product-image" src={"/images/" + product.brand.toLowerCase() + ".png"}
                            alt={product.brand} />
                    </div>
                    <div className="p-col-12 product-data" >
                        <div className="product-title" > {product.productName} </div>
                        <div className="product-subtitle" > {product.price}/- </div>
                    </div>
                </div>
            </NavLink>
        );
    }

    shuffleArray = (array) => {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    render() {
      
        let tempProducts = [];
        console.log("in rendereee");
        console.log(this.state.products);
        Object.values(this.state.products).map(el => {
          
                tempProducts.push(el);
           
         
            this.shuffleArray(tempProducts);
        });
        console.log("tempProducts");
        console.log(tempProducts);
        

        return (
            //    <UserConsumer>{(user) => {
            //      return  (user.username)
            //     }
            //     }</UserConsumer> 
            <Carousel
                value={tempProducts}
                itemTemplate={this.productTemplate}
                numVisible={4}
                numScroll={1}
                className="custom-carousel"
                circular={true}
                autoplayInterval={3000} >
            </Carousel>
        );
    }
}

export default HomePage;