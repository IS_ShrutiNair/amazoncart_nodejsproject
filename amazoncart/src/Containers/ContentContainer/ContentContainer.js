import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import HomePage from '../HomePage/HomePage';
import CategoryPage from '../CategoryPage/CategoryPage';
import Navigation from '../../Components/Navigation/Navigation';

class ContentContainer extends Component {
    state = {
        navClick: true
    }

    clickHandler = () => {
        this.setState({
            navClick: !this.state.navClick
        })
    }

    render() {
        let content;
        if (this.props.location.pathname == '/home') {
            content = <HomePage />
        }
        if (this.props.location.pathname.startsWith('/category')) {
            content = <CategoryPage />
        }

        return (
            <div className="p-grid main-layout">              
                    <Navigation clicked={this.clickHandler} />              
                <div className="p-col-12 p-md-12  main-content">
                    {content}
                </div>
            </div>
        );
    }
}

export default withRouter(ContentContainer);