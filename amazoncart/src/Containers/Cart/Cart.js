import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../../Store/actions';
import axios from 'axios';
import './Cart.css';
import Button from '../../Components/Button/Button';
import Swal from 'sweetalert2';

class Cart extends Component {
    constructor(props) {
        super();
        this.state = {
            allProductData: [],
            btnDisabled:true
        }
    }

    componentDidMount() {
        axios.get('http://localhost:9000/')
            .then((response) => {
                this.setState({
                    allProductData: response.data
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    deleteHandler = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: 'This item will be deleted!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Item has been deleted..!',
                    'success'
                )
                this.props.deleteItemHandler(id);
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire(
                    'Cancelled',
                    'Item is still in cart!'
                )
            }
        })
    }
    onPlaceOrder = (username) => {
        Swal.fire({
            title: 'Are you sure?',
            text: 'Order will be placed!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, place order!',
            cancelButtonText: 'No, discard it'
        }).then((result) => {
            if (result.value) {
                Object.entries(this.props.itemCount).map((el)=>{
                    console.log(el);
                })
                axios.post('http://localhost:9000/placeOrderApi',{
                    username:username,
                    cart:this.props.itemCount
                })
                .then((response) => {
                    Swal.fire(
                        'Order Placed Successfully!',
                        'Order has been placed..!',
                        'success'
                    )
                    this.props.clearCartHandler();  
                    console.log(this.props.history);
                    this.props.history.push('/order');
                })
                .catch(function (error) {
                    console.log(error);
                })               
              
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire(
                    'Cancelled',
                    'Items are still in cart!'
                )
            }
        })
    }

    updateHandler = (id, quantity) => {
        Swal.fire({
            title: 'Are you sure?',
            text: 'This item will be updated!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, update it!',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Updated!',
                    'Item has been updated..!',
                    'success'
                )
                if (quantity.trim() == "")
                    quantity = 0;
                this.props.updateItemHandler(id, quantity);  

            } else if (result.dismiss === Swal.DismissReason.cancel) {
                document.getElementById("updateTextBox" + id).value = this.props.itemCount[id];
                Swal.fire(
                    'Cancelled'
                )
            }
            this.setState({
                btnDisabled:true
            })
        })
    }

    render() {
        let cartData = [];
        let items = null;
        let totalPrice = [0];
         console.log("this.props.cart");
         console.log(this.props.cart);
         this.state.allProductData.map((el1) => {                     
                if (this.props.cart.includes(el1.productid))
                cartData.push(el1);           

        });        

        if (cartData[0]) {
            console.log("this.props.itemCount");
            console.log(this.props.itemCount);
                items = <>
                    <table className="Cart container">
                        <thead>
                            <tr className="row">
                                <th className="col-md-3 ">Product</th>
                                <th className="col-md-2 ">Product Name </th>
                                <th className="col-md-2 ">Price  </th>
                                <th className="col-md-2 ">Quantity  </th>
                                <th className="col-md-3 ">  </th>
                            </tr>
                        </thead>
                        <tbody>
                            {cartData.map(item => {
                                totalPrice.push(item.price * this.props.itemCount[item.productid])
                                return <tr className="row" key={item.productid}>
                                    <td className="col-md-3">
                                        <img className="cart-image" src={'/images/' + (item.brand.toLowerCase()) + '.png'} alt={item.productName} />
                                    </td>
                                    <td className="col-md-2">
                                        <p>{item.productName}</p>
                                    </td>
                                    <td className="col-md-2">
                                        <p>{
                                            item.price * this.props.itemCount[item.productid]
                                        }/-</p>
                                    </td>
                                    <td className="col-md-2">
                                        <input type="text" id={"updateTextBox" + item.productid} className="form-controls" onChange={()=>{this.setState({btnDisabled:false})}} defaultValue={this.props.itemCount[item.productid]} />
                                    </td>
                                    <td className="col-md-3">
                                        <Button displayText="Update" disabled= {this.state.btnDisabled} clicked={() => this.updateHandler(item.productid, document.getElementById('updateTextBox' + item.productid).value)}></Button>
                                        <Button displayText="Delete" clicked={() => this.deleteHandler(item.productid)}></Button>
                                    </td>
                                    <hr />
                                </tr>
                            })}
                        </tbody>
                    </table>
                    <p className="col-md-4 col-md-offset-8"><b>Total Price: {
                        totalPrice.reduce((accumulator, currentvalue = 0) => {
                            return parseInt(accumulator + currentvalue)
                        })}/-
                    </b>
                    </p>
                    <div className="btn-submit login-input  col-md-4 col-md-offset-8">
                        <input className="btn btn-color" type="button" value="Place Order" onClick={e => this.onPlaceOrder(sessionStorage.getItem('username'))} />
                    </div>
                </>
            
        }
        else {
            items = <h3>Cart is Empty!</h3>
        }

        return (items);
    }
}
const mapStateToProps = (state) => {
    return {
        cart: state.cart,
        itemCount: state.itemCount
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        deleteItemHandler: (id) => dispatch(actionCreators.deleteItem(id)),
        updateItemHandler: (id, quantity) => dispatch(actionCreators.updateItem(id, quantity)),
        clearCartHandler: () => dispatch(actionCreators.clearCart())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);