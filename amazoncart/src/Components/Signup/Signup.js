import React , {useRef} from 'react';

const Signup = () => {

    const inputEl = useRef(null);
    const inputEl2 = useRef(null);

    function mouseoutPass() {
        inputEl.current.type = "password";
        inputEl2.current.type = "password";
    }
    function mouseoverPass() {
        inputEl.current.type = "text";
        inputEl2.current.type = "text";
    }

    return (      
            <div className="login-page">
                <p className="login-head"><b>Sign Up</b></p>
                <div className="jumbotron inner-box">
                    <form action="http://localhost:9000/newUserApi/" method="post">
                        <div className="login-input col-md-12">
                            <input type="text" className="form-control" placeholder="Full Name" name="fullname" required />
                        </div>
                        <div className="login-input col-md-12">
                            <input type="text" className="form-control" placeholder="Username i.e.EmailId" name="username" required />
                        </div>
                        <div className="login-input col-md-12">
                            <input type="password" ref={inputEl} className="form-control" id="password"  name="password" placeholder="Password"  required />
                            <span id="eye-password" onMouseOver={e => mouseoverPass(e)} onMouseOut={e => mouseoutPass(e)}><i className="fa fa-eye" aria-hidden="true" ></i></span>
                        </div>
                        <div className="login-input col-md-12">
                            <input type="password" ref={inputEl2} className="form-control" id="password" name="confirm-password" placeholder="Confirm Password"  required />
                            <span id="eye-password" onMouseOver={e => mouseoverPass(e)} onMouseOut={e => mouseoutPass(e)}><i className="fa fa-eye" aria-hidden="true" ></i></span>
                        </div>
                        <div className="btn-submit login-input col-md-12">
                            <input className="btn btn-color" type="submit" value="Submit" id="submitBtn" />
                        </div>
                    </form>                   
                </div>
            </div>       
    );
};

export default Signup;