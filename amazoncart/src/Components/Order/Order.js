import React, { Component } from 'react';
import Axios from 'axios';
import './Order.css';
class Order extends Component {
    state = {
        orderData: []
    }

    componentDidMount() {
        let username = (sessionStorage.getItem('username'));
        Axios.get('http://localhost:9000/getOrderApi/' + username)
            .then((response) => {
                console.log(response);
                this.setState({
                    orderData: response.data
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    invoiceHandler = (id) => {
        console.log(id);
        Axios.get('http://localhost:9000/invoiceApi/' + id)
            .then((response) => {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });

    }
    render() {
        let Order;
        if (this.state.orderData.length <= 0)
            Order = <h4>Nothing there!</h4>

        else {
            Order = <ul className="orderItems">
                {this.state.orderData.map((el, index) => {
                    let date = new Date(el.date);
                    return <li className="order-item">
                        <h4>Order - #{el._id} - <a className="invoicelink" href={"http://localhost:9000/invoiceApi/" + el._id}>Invoice</a></h4>
                        <span>Order placed on - {date.toString().split('G')[0]}</span>
                        <hr/>
                        <ul className="orderProducts">
                            {el.products.map((el, index) => {
                                return <li className="order-product">
                                    <h5>{el.productName} - ({el.quantity}) </h5>
                            <span>Rs. {el.price *  el.quantity}/-</span>
                                </li>
                            })}
                        </ul>
                    </li>
                })}
            </ul>

        }
        return (
            <div>
                {Order}

            </div>
        );
    }
}

export default Order;