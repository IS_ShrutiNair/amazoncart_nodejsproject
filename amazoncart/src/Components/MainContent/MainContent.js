import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import './MainContent.css';

class MainContent extends Component {

    eachProductClicked = (id) => {
        this.props.clicked(id);
    }

    render() {
        let productData = null;
        if (this.props.data.length > 0) {
            productData = this.props.data.map((eachproduct) => {
                            return <div className="p-col-3 product-list" key={eachproduct.productid}>
                                    <NavLink to={this.props.location.pathname + '/' + eachproduct.productid} key={eachproduct.productid}>
                                        <div  onClick={() => this.eachProductClicked(eachproduct.productid)}>
                                            <div onClick={eachproduct.clicked}>
                                                <img  className="product-image" src={ eachproduct.imageUrl} alt={eachproduct.productName} />
                                                <h3>{eachproduct.productName}</h3>
                                                <p><b>Price:</b>{eachproduct.price}/-</p>
                                            </div>
                                        </div>
                                    </NavLink>
                                    </div>
                             });
        }

        return (          
                <div className="p-col-12 p-col-nogutter" >
                    <span className="heading">{this.props.heading}</span>
                    <hr />
                    <div className="p-grid">
                        {productData}
                    </div>
                </div>          
        );
    }
}

export default withRouter(MainContent);