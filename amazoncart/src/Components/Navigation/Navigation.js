import React, { useState } from 'react';
import "primeflex/primeflex.css";
import './Navigation.css';
import { NavLink, withRouter } from 'react-router-dom';
import { Sidebar } from 'primereact/sidebar';
import "primeflex/primeflex.css";
import 'primeicons/primeicons.css';
import {Button} from 'primereact/button';
import 'primereact/resources/themes/nova-light/theme.css';
const Navigation = (props) => {
    const [visible, setVisible] = useState("");
    return (
        <>
            <Sidebar visible={visible} position="left" style={{width:'30em'}} onHide={(e) => setVisible(false)}>
                <ul className="ul-nav text-center">
                    <li className="li-nav">
                        <NavLink exact={true} to="/category/mobiles" activeClassName="active-nav" onClick={props.clicked}>
                            Mobiles
                        </NavLink>
                    </li>
                    <li className="li-nav">
                        <NavLink exact={true} to="/category/laptops" activeClassName="active-nav">
                            Laptops
                        </NavLink>
                    </li>
                    <li className="li-nav">
                        <NavLink exact={true} to="/category/cameras" activeClassName="active-nav">
                            Cameras
                        </NavLink>
                    </li>
                </ul>
            </Sidebar>
            <Button icon="pi pi-bars" className="burgerbar" onClick={(e) => setVisible(true)} />
        </>

    );

};

export default withRouter(Navigation);