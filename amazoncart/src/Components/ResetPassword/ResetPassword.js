import React, { useRef } from 'react';
import queryString from 'query-string';
import FlashMessage from 'react-flash-message';
import './ResetPassword.css';

const ResetPassword = (props) => {
    console.log(props);
    let url = props.location.search;
    let params = queryString.parse(url);
    console.log(params.reset);

    let message = (params.reset == undefined) ? null : ((params.reset == 'true') ?
        <FlashMessage persistOnHover={false} duration={3000}>
            <div className="flash col-md-offset-4" >
                <strong>Reset link sent successfully..Please check your email address !!</strong>
            </div>
        </FlashMessage>
        :
        <FlashMessage persistOnHover={false} duration={3000}>
            <div className="flash col-md-offset-4" >
                <strong>No account with that email-id found !!</strong>
            </div>
        </FlashMessage>);

    return (
        <div className="login-page">
            <p className="login-head"><b>Reset Password</b></p>
            <div className="jumbotron inner-box">
                <form action="http://localhost:9000/resetPasswordApi/" method="post">

                    <div className="login-input col-md-12">
                        <input type="text" className="form-control" placeholder="EmailId" name="email" required />
                    </div>

                    <div className="btn-submit login-input col-md-12">
                        <input className="btn btn-color" type="submit" value="Reset Password" id="submitBtn" />
                    </div>
                </form>
            </div>
            {message}
        </div>
    );
};

export default ResetPassword;