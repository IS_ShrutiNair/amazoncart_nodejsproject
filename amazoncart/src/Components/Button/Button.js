import React from 'react';
import './Button.css';

const Button = (props) => {
    return (
        <div className="button-container">
            <input type="button" className="btn btn-primary btn-color" id={props.id + "_btn"} value={props.displayText} onClick={props.clicked} disabled = {props.disabled}></input>
        </div>
    );
};

export default Button;