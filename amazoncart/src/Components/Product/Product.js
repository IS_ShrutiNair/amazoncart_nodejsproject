import React, { Component } from 'react';
import axios from 'axios';
import { NavLink, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import Button from '../Button/Button';

class Product extends Component {
    constructor() {
        super();
        this.state = {
            mobiles: [],
            laptops: [],
            cameras: [],
            showText: 'Show More...'

        }
    }

    componentDidMount() {
        axios.get('http://localhost:9000/')
            .then((response) => {
                var mob = [];
                var lap = [];
                var cam = [];
                console.log(response.data);
                response.data.map((el) => {
                    if (el.productid.startsWith('cam')) {
                        cam.push(el);
                    }
                    if (el.productid.startsWith('mob')) {
                        mob.push(el);
                    }
                    if (el.productid.startsWith('lap')) {
                        lap.push(el);
                    }
                });

                this.setState({
                    mobiles: mob,
                    laptops: lap,
                    cameras: cam
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    show = () => {
        console.log("showText")
    }
  
    onDeleteClick = (id) => {
        axios.post('http://localhost:9000/deleteApi',
            {
                pid: id
            })
            .then((response) => {
                console.log(response.data);
                console.log("deleted");
                if (response.data) {

                    Swal.fire("Product Deleted")
                        .then((result) => {
                            if (result.value) {
                                window.location.reload(true);
                            }
                        });
                }
            })
            .catch(function (error) {
                console.log(error);
            })
    }



    render() {
        var allDataArray = [...this.state.cameras, ...this.state.laptops, ...this.state.mobiles]
        console.log('in array');
        console.log(this.state.mobiles);
        console.log(this.state.laptops);
        console.log(this.state.cameras);
        console.log(allDataArray);


        let productData = null;
        if (allDataArray.length > 0) {
            productData = allDataArray.map((eachproduct) => {
                return <div className="p-col-2 product-list" key={eachproduct.productid}>
                    <div>
                        <img className="product-image" src={ eachproduct.imageUrl} alt={eachproduct.productName} />
                        <h3>{eachproduct.productName}</h3>
                        <p><b>Price:</b>{eachproduct.price}/-</p>
                        <p><b>Brand:</b>{eachproduct.price}</p>
                        <p><b>Product Description:</b>{(eachproduct.productDescription).split('.')[0]} <a onClick={this.show}>{this.state.showText}</a></p>
                    </div>
                    <div className="btn-submit login-input col-md-6">
                        <a className="btn btn-color"  href={"/editproduct/"+eachproduct.productid} >Edit</a>
                    </div>
                    <div className="btn-submit login-input col-md-6">
                        <input className="btn btn-color" type="button" value="Delete" onClick={e => this.onDeleteClick(eachproduct._id)} />
                    </div>
                </div>
            });
        }

        return (
            <div className="p-col-12 p-col-nogutter" >

                <span className="heading">{this.props.heading}</span>
                <hr />
                <div className="p-grid">
                    {productData}
                </div>

            </div>
        );
    }
}

export default Product;