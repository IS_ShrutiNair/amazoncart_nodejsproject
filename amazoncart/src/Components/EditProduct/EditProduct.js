import React, { Component } from 'react';
import Axios from 'axios';

class EditProduct extends Component {
    constructor(props) {
        super();
        this.state = {
            value: 0,
            productData: []
        }
    }
    componentDidMount() {
        Axios.get('http://localhost:9000/' + this.props.match.params.id)
            .then((response) => {
                console.log("response.data  for get id");
                console.log(response.data);
                this.setState({
                    productData: response.data
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    onChangeHandler = (event) => {
        console.log("Changed", event.target.value);
        this.setState({
            value: event.target.value
        })
    }
    render() {
        let category = 'Select';
        console.log(this.state.productData);
        console.log(this.state.productData);
       

        if (this.state.productData.length != 0) {
            if (this.state.productData.productid.slice(0, 3) == 'cam') {
                category = 'Camera';
            }
            if (this.state.productData.productid.slice(0, 3) == 'mob') {
                category = 'Mobile';
            }
            if (this.state.productData.productid.slice(0, 3) == 'lap') {
                category = 'Laptop';
            }
           var data = <form action="http://localhost:9000/editProductApi/" method="POST" encType="multipart/form-data">
            <div className="col-md-12 input-product">
                <select type="text" className="form-control" placeholder="Product Category" name="category" value={category} required onChange={(e) => this.onChangeHandler(e)} required>
                    <option>Select</option>
                    <option>Camera</option>
                    <option>Laptop</option>
                    <option>Mobile</option>
                </select>
            </div>
            <div className="col-md-12 input-product">
                <input type="text" className="form-control" placeholder="Product Name" name="pname" defaultValue={this.state.productData.productName} required />
            </div>
            <div className="col-md-12 input-product">
                <input type="text" className="form-control" placeholder="Product Brand" name="pbrand" defaultValue={this.state.productData.brand} required />
            </div>
            <div className="col-md-12 input-product">
                <input type="file" className="form-control" name="pimage"  />
            </div>
            <div className="col-md-12 input-product">
                <input type="number" className="form-control" placeholder="Price" name="pprice"  defaultValue={this.state.productData.price} required />
            </div>
            <div className="col-md-12 input-product">
                <textarea className="form-control" placeholder="Product Description" name="pdesc"  defaultValue={this.state.productData.productDescription} required />
            </div>
            <div className="col-md-12 input-product hidden">
                <input type="text" className="form-control" name="pid" value={this.props.match.params.id} required />
            </div>

            <div className="btn-submit col-md-12">
                <input className="btn btn-color" type="submit" value="Edit Product" id="submitBtn" />
            </div>
        </form>
        }
        return (
            <div className="add-product">
                {
                    data
                }
            </div>
        );
    }


}
export default EditProduct;