import React, { useRef } from 'react';
import queryString from 'query-string';
import FlashMessage from 'react-flash-message';
import './SetPassword.css';
import { Redirect } from 'react-router';

const SetPassword = (props) => {
    console.log(props);
    let token = props.match.params;
    console.log(token.token.length);
    let message = null;
    if (token.token.length == 65) {
         message = <FlashMessage persistOnHover={false} duration={3000}>
            <div className="flash col-md-offset-4" >
                <strong>Password updated successfully !!</strong>
            </div>
        </FlashMessage>        
    }
    else if(token.token == 0){
        message = <FlashMessage persistOnHover={false} duration={3000}>
            <div className="flash col-md-offset-4" >
                <strong>Not a valid link !!</strong>
            </div>
        </FlashMessage>
    }

    return (
        <div className="login-page">
            <div className="jumbotron inner-box">
                <form action="http://localhost:9000/setNewPasswordApi/" method="post">
                    <div className="login-input col-md-12">
                        <label>New password</label>
                        <input type="text" className="form-control" placeholder="New Password" name="newpassword" required />
                    </div>
                    <div className="login-input col-md-12 hidden">
                        <input type="text" className="form-control" name="token" value={token.token} required />
                    </div>
                    <div className="btn-submit login-input col-md-12">
                        <input className="btn btn-color" type="submit" value="Set Password" id="submitBtn" />
                    </div>
                    <div className="col-md-12">
                    <a className="pull-right" href="/login" >Login</a>
                </div>
                </form>
            </div>
            {message}
        </div>
    );
};

export default SetPassword;