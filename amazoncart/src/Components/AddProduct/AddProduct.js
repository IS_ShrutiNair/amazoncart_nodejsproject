import React, { Component } from 'react';
import './AddProduct.css';
class AddProduct extends Component {
    state = {
        value: 0
    }
    onChangeHandler = (event) => {
        console.log("Changed", event.target.value);
        this.setState({
            value: event.target.value
        })
    }
    render() {
        return (
            <div className="add-product">
                <form action="http://localhost:9000/addProductApi/" method="POST" encType = "multipart/form-data">
                    <div className="col-md-12 input-product">
                        <select type="text" className="form-control" placeholder="Product Category" name="category" required onChange={(e) => this.onChangeHandler(e)} required>
                            <option>Select</option>
                            <option>Camera</option>
                            <option>Laptop</option>
                            <option>Mobile</option>
                        </select>
                    </div>
                    <div className="col-md-12 input-product">
                        <input type="text" className="form-control" placeholder="Product Name" name="pname" required />
                    </div>
                    <div className="col-md-12 input-product">
                        <input type="text" className="form-control" placeholder="Product Brand" name="pbrand" required />
                    </div>
                    <div className="col-md-12 input-product">
                        <input type="file" className="form-control" name="pimage" required />
                    </div>
                    <div className="col-md-12 input-product">
                        <input type="number" className="form-control" placeholder="Price" name="pprice" required />
                    </div>
                    <div className="col-md-12 input-product">
                        <textarea className="form-control" placeholder="Product Description" name="pdesc" required />
                    </div>
                    <div className="col-md-12 input-product hidden">
                        <input  type="text" className="form-control" name="pid" value={(this.state.value).toString().toLowerCase().slice(0, 3) + parseInt(Math.random() * 100)} required />
                    </div>
                    <div className="btn-submit col-md-12">
                        <input className="btn btn-color" type="submit" value="Submit" id="submitBtn" />
                    </div>
                </form>
            </div>
        );
    }


}
export default AddProduct;