import React, { useState, useRef, useEffect } from 'react';
import './Login.css';
import { connect } from 'react-redux';
import * as actionCreators from '../../Store/actions';
import axios from 'axios';

const Login = (props) => {
    const myRef = React.createRef();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(<p></p>);
    const inputEl = useRef(null);

    useEffect(() => {
        myRef.current.focus();
    }, []);


    function handleLogin(username) {
        axios.post('http://localhost:9000/loginApi/', {
            username: username
        })
            .then((response) => {
                console.log("response in loginapi");
                console.log(response.data.length != 0);
                if (response.data.length !== 0) {
                    sessionStorage.setItem('username', response.data[0].username);
                    sessionStorage.setItem('fullname', response.data[0].fullname);
                    props.loggedInHandler();
                    if (response.data[0].username !== "admin@infostretch.com") {
                        if (sessionStorage.getItem('username') != null)
                            props.history.push('/home');
                        else
                            props.history.push('/');
                    }
                    else {
                        if (sessionStorage.getItem('username') != null)
                            props.history.push('/product');
                        else
                            props.history.push('/');
                    }
                }
                else {
                    setError(<span className="error">Please check the credentials!</span>)
                }
            })
            .catch(function (error) {
                console.log(error);
            })

    }

    function mouseoutPass() {
        inputEl.current.type = "password";
    }
    function mouseoverPass() {
        inputEl.current.type = "text";
    }
    function validateForm() {
        return username.length > 0 && password.length > 0;
    }
    return (
        <div className="login-page">
            <p className="login-head"><b>Amazon Cart</b></p>
            <div className="jumbotron inner-box">
                <div className="login-input col-md-12">
                    <input type="text" className="form-control" placeholder="Username" value={username} ref={myRef} onChange={e => setUsername(e.target.value)} required />                </div>
                <div className="login-input col-md-12">
                    <input type="password" ref={inputEl} className="form-control" id="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required />
                    <span id="eye-password" onMouseOver={e => mouseoverPass(e)} onMouseOut={e => mouseoutPass(e)}><i className="fa fa-eye" aria-hidden="true" ></i></span>

                </div>
                <div className="btn-submit login-input col-md-12">
                    <input className="btn btn-color" type="button" value="Login" id="LoginBtn" disabled={!validateForm()} onClick={() => handleLogin(username)} />
                </div>
                <div className="col-md-6">
                    <a className="" href="/resetpassword" >Forgot password?</a>
                </div>
                <div className="col-md-6">
                    <a className="pull-right" href="/signup" >New user? sign up here...</a>
                </div>
                {error}
            </div>
        </div>

    );
};
const mapDispatchToProps = (dispatch) => {
    return {
        loggedInHandler: () => dispatch(actionCreators.isLoggedIn(sessionStorage.getItem('username') != null))
    }

}
const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.isLoggedIn

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);