import React from 'react';
import './Layout.css';
import Header from '../Header/Header';
import { connect } from 'react-redux';
import { withRouter, Route, Redirect } from 'react-router-dom';

const Layout = (props) => {
    console.log("props.isLoggedIn");
    console.log(props.isLoggedIn);
    return (
        <div className="layout">
            {/*this route can be written in app also, but it is written here due to the props.isloggedin value* */}
            <Route exact path="/" render={() => (
                props.isLoggedIn ? (
                    (sessionStorage.getItem('username') == 'admin@infostretch.com') ? <Redirect to="/product" /> : <Redirect to="/home" />
                ) : (
                        <Redirect to="/login" />
                    )
            )} />
            <Route exact path="/login" render={() => (
                props.isLoggedIn ? (
                    <Redirect to="/home" />
                ) : (
                        <Redirect to="/login" />
                    )
            )} />
            {props.isLoggedIn && (sessionStorage.getItem('username') != null) && <Header />}
            {props.children}
        </div>
    );

}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.isLoggedIn

    }
}
export default connect(mapStateToProps)(withRouter(Layout));