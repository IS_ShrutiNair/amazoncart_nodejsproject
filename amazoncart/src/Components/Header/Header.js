import React from 'react';
import './Header.css';
import { connect } from 'react-redux';
import { withRouter, NavLink } from 'react-router-dom';

const Header = (props) => {

    const logout = () => {
        sessionStorage.removeItem('username');
        if (sessionStorage.getItem('username') === null)
            window.location.href = '/';
    }
    return (
        <header className="Header row">
            {(sessionStorage.getItem('username') === 'admin@infostretch.com') ?
                <NavLink to="/product" className="logo-text" title="Home">
                    <figure>
                        <img src="/images/logo.png" alt="AMAZON CART" width="150" height="100" />
                    </figure>
                </NavLink> :
                <NavLink to="/home" className="logo-text" title="Home">
                    <figure>
                        <img src="/images/logo.png" alt="AMAZON CART" width="150" height="100" />
                    </figure></NavLink>}
            <div className="header-right">
                {(sessionStorage.getItem('username') === 'admin@infostretch.com') ?
                    (<span><NavLink to="/addproduct" className="nav-header-products" title="Add Product">Add Product</NavLink>
                        <NavLink to="/product" className="nav-header-products" title="Products">Products</NavLink></span>) :
                    null}
                <span className="username">Hi {sessionStorage.getItem("fullname")}</span>
                <NavLink to="/cart" className="carticon" ><i className="fa fa-shopping-cart fa-lg" aria-hidden="true"></i><span className="item-count">{props.cart.length}</span></NavLink>
                <span className="main-logout btn" title="Logout" onClick={logout}><i className="fa fa-power-off" aria-hidden="true" ></i></span>
            </div>
        </header>
    );
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart,
        isLoggedIn: state.isLoggedIn
    }
}


export default connect(mapStateToProps)(withRouter(Header));