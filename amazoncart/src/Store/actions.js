export const IS_LOGGED_IN = 'IS_LOGGED_IN';
export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_ITEM = 'DELETE_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';
export const CLEAR_CART = 'CLEAR_CART';


/*Action creators */
export const isLoggedIn = (val) => {
    return {
        type: IS_LOGGED_IN,
        isLoggedIn:val
    }
}

export const addToCart = (id) => {
    return {
        type: ADD_TO_CART,
        id:id
    }
}
export const deleteItem = (id) => {
    return {
        type: DELETE_ITEM,
        id:id
    }
}
export const updateItem = (id,quantity) => {
    return {
        type: UPDATE_ITEM,
        id:id,
        quantity:quantity
    }
}
export const clearCart = () => {
    return {
        type: CLEAR_CART       
    }
}


