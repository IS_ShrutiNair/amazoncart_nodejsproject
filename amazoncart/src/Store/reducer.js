import * as actionType from './actions';

const initialState = {
    isLoggedIn: sessionStorage.getItem('username') != null,
    cart: [],
    itemCount: {     
    }
}
const reducer = (state = initialState, actions) => {
    // console.log("state");
    // console.log(state);
    switch (actions.type) {
        case actionType.IS_LOGGED_IN: {
            return {
                ...state,
                isLoggedIn: actions.isLoggedIn
            }
        }

        case actionType.ADD_TO_CART: {  
         //   console.log(typeof(state.itemCount[actions.id]));        
            if (state.itemCount[actions.id] == undefined) {
                return {
                    ...state,
                    cart: [
                        ...state.cart,
                        actions.id
                    ],
                    itemCount: {
                        ...state.itemCount,
                        [actions.id]: 1

                    }
                }
            }
            else {
                return {
                    ...state,
                    cart: [
                        ...state.cart,
                        actions.id
                    ],
                    itemCount: {
                        ...state.itemCount,
                        [actions.id]: state.itemCount[actions.id] + 1

                    }
                }
            }


        }

        case actionType.DELETE_ITEM: {
            return {
                ...state,
                cart: state.cart.filter((item) => item !== actions.id),
                itemCount: {
                    ...state.itemCount,
                    [actions.id]: 0

                }
            }
        }
        case actionType.CLEAR_CART: {
            return {
                ...state,
                cart:[],
                itemCount: {   }
            }
        }

        case actionType.UPDATE_ITEM: {
            let number = (actions.quantity - state.itemCount[actions.id]) > 0 ? 1 : 0;
            let array = state.cart.filter((item) => item !== actions.id);

            if (number) {
                return {
                    ...state,
                    cart: [
                        ...state.cart.concat((Array(actions.quantity - state.itemCount[actions.id]).fill(actions.id)))

                    ],
                    itemCount: {
                        ...state.itemCount,
                        [actions.id]: parseInt(actions.quantity)
                    }
                }
            }
            else {
                return {
                    ...state,
                    cart: [
                        ...array.concat((Array(parseInt(actions.quantity)).fill(actions.id)))
                    ],
                    itemCount: {
                        ...state.itemCount,
                        [actions.id]: parseInt(actions.quantity)
                    }
                }
            }


        }
    }
    return state;
}
export default reducer;